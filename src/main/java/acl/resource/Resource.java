package acl.resource;

import acl.access.AccessControlList;
import acl.access.AccessRight;
import acl.user.User;

public abstract class Resource {

	protected Directory parent;

	private AccessControlList acl;

	public String name = "";

	public Resource(String name) {

		this.name = name;

	}

	public void setACL(AccessControlList acl) {

		this.acl = acl;
	}

	public String accessBy(User user) {

		Resource tempR = this;

		while (tempR != null) {
			
			if (tempR.acl == null) {
				tempR = tempR.parent;

			} else {
				
				switch (tempR.acl.getAccessRightFor(user)) {
				
				case GRANTED:
					return this.getContent();

				case DENIED:
					return null;
					
				// Not included in test
				case UNSPECIFIED:
					tempR = tempR.parent;
					break;

				default:
					break;
				}
			}
		}
		return null;
	}

	public abstract String getContent();

	public String getName() {

		return name;
	}

	public void setParent(Directory parent) {

		this.parent = parent;
	}

}
