package acl.resource;

import java.util.ArrayList;

public class Directory extends Resource {

	String content = "";

	private ArrayList<Resource> children = new ArrayList<Resource>();

	public Directory(String name) {

		super(name);

	}

	public void add(Resource resource) {

		resource.setParent(this);

		children.add(resource);

	}

	@Override
	public String getContent() {

		for (Resource temp : children) {

			content += temp.getName() + "\n";
		}

		return content;
	}
}