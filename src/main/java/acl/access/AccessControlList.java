package acl.access;

import java.util.ArrayList;

import acl.user.User;

import acl.access.AccessRight;

public class AccessControlList {

	private ArrayList<AccessControlEntity> ace = new ArrayList<AccessControlEntity>();

	public void add(AccessControlEntity ace) {

		this.ace.add(ace);
	}

	public AccessRight getAccessRightFor(User user) {
		
		AccessRight aR = AccessRight.UNSPECIFIED;
		
		for (AccessControlEntity ace : this.ace) {
			
			if (ace.getUser() != user)
				return AccessRight.DENIED;
			
			if (ace.getAccessRight() == AccessRight.DENIED)
				return AccessRight.DENIED;
			
			if (ace.getAccessRight() == AccessRight.GRANTED)
				aR = AccessRight.GRANTED;
		}
		
		return aR;
		
	}
}
